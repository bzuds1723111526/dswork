#include <iostream>
#include "linklist.h"

using namespace std;

int main()
{
    LinkList<int> L;

    InitList(L);

    ListInsert(L, 1, 10);
    ListInsert(L, 1, 20);
    ListInsert(L, 1, 30);
    ListInsert(L, 1, 40);
    ListInsert(L, 1, 50);
    
    int x;
    ListDelete(L, 3, x);
    cout << x << "deleted" << endl;
    
    for (auto p = L->next; p; p = p->next)
        cout << p->data << endl;
    
    return 0;
}