#include <iostream>
#include "binarytree.h"


using namespace std;

int main()
{
    //      -
    // a         *
    //         b  c
    auto pa = new BiTNode<char>{'a',nullptr, nullptr};
    auto pb = new BiTNode<char>{'b',nullptr, nullptr};
    auto pc = new BiTNode<char>{'c',nullptr, nullptr};
    auto pm = new BiTNode<char>{'*', pb, pc};
    auto ps = new BiTNode<char>{'-',pa, pm};

    auto T = ps;
    auto print = [](char c) { cout << c; };

    Preorder(T, print);
    cout << endl;

    Inorder(T, print);
    cout << endl;

    Postorder(T, print);
    cout << endl;

    Print(T);

    cout << "NodeCount: " << NodeCount(T) << endl;
    cout << "LeafCount: " << LeafCount(T) << endl;
    cout << "Depth: " << Depth(T) << endl;

    // 建立二叉树
    T = CreateBinaryTree();
    Print(T);

    cout << "Destroy" << endl;
    Destroy(T);
    Print(T);

    BiTree<int> bst = nullptr;
    int a[] = {13, 24, 37, 90, 53, 40, 20};
    for (int x : a)
         InsertBST(bst, x);

    Print(bst);

    cout << "Min: " << FindMinBST(bst)->data << endl;
    cout << "Max: " << FindMaxBST(bst)->data << endl;
    
    cout << "Delete " << 40 << endl;
    DeleteBST(bst, 40);
    Print(bst);
    
    cout << "Delete " << 37 << endl;
    DeleteBST(bst, 37);
    Print(bst);

    cout << "Delete " << 24 << endl;
    DeleteBST(bst, 24);
    Print(bst);


    Destroy(bst);


    return 0;
}